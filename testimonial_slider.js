(function ($) {
	function testimonialAutoHeight(curr, next, opts, fwd) {
		var $ht = jQuery(this).height();

		//set the container's height to that of the current slide
		jQuery(this).parent().animate({height: $ht});
	}
	Drupal.behaviors.testimonialSlider = {
		attach: function (context, settings) {
			jQuery('#testimonial-slider')
			.cycle({
				fx: 'scrollVert', // choose your transition type, ex: fade, scrollUp, scrollRight, shuffle
				after: testimonialAutoHeight,
			});
    	}
	};
}(jQuery));